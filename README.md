# Окружение #
* IDE: [Dev-C++](http://www.bloodshed.net/devcpp.html)
* Фреймворк: [SDL](http://www.libsdl.org)
* CVS: Git [console](http://git-scm.com) / [portable (.exe)](http://portableapps.com/node/34685) / [portable (source)](https://github.com/bungeshea/GitPortable)
* Git GUI: [Source Tree](http://www.sourcetreeapp.com)

# Информация #
## По фреймворку ##
* [Цикл статей (английский)](http://lazyfoo.net/tutorials/SDL/index.php) (*предпочтительней*)
* [Цикл статей (русский)](http://habrahabr.ru/post/166875/)

## По Пакману ##
* [Pac-Man Dossier (Досье Пакмана)](http://home.comcast.net/~jpittman2/pacman/pacmandossier.html)
* [Алгоритм поведения привидений в игре Pac-Man](http://habrahabr.ru/post/109406/)

## По инструментам ##
* [Книга по Git (Русский)](http://git-scm.com/book/ru/v1)
* [Книга по Git (Английский)](http://git-scm.com/book/en/v2)
* [Разработка на основе распределенного Git (Русский)](http://git-scm.com/book/ru/v1/Распределённый-Git-Распределённые-рабочие-процессы)
* [Разработка на основе распределенного Git (Английский)](http://git-scm.com/book/en/v2/Distributed-Git-Distributed-Workflows)

## Теория разработки игр ##
* [Шаблоны игрового проектирования (Русский)](http://live13.livejournal.com/462582.html)
* [Шаблоны игрового проектирования (Английский)](http://gameprogrammingpatterns.com)